import { Icon, IconProps } from "@chakra-ui/react";

export const HeartIcon = ({
  w = 6,
  h = 6,
  stroke = "gray.500",
  ...props
}: IconProps) => (
  <Icon
    w={w}
    h={h}
    fill="none"
    strokeWidth={2}
    stroke={stroke}
    {...props}
    viewBox="0 0 24 24"
  >
    <path
      strokeWidth="2"
      d="M4.45 13.908l6.953 6.531c.24.225.36.338.5.366a.5.5 0 00.193 0c.142-.028.261-.14.5-.366l6.953-6.53a5.203 5.203 0 00.549-6.983l-.31-.399c-1.968-2.536-5.918-2.111-7.301.787a.54.54 0 01-.974 0C10.13 4.416 6.18 3.99 4.212 6.527l-.31.4a5.203 5.203 0 00.549 6.981z"
    ></path>
  </Icon>
);
