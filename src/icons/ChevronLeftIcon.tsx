import { Icon, IconProps } from "@chakra-ui/react";

export const ChevronLeftIcon = ({
  w = 6,
  h = 6,
  stroke = "gray.500",
  ...props
}: IconProps) => (
  <Icon w={w} h={h} stroke={stroke} {...props} fill="none" viewBox="0 0 24 24">
    <path strokeWidth="2" d="M15 6l-6 6 6 6" />
  </Icon>
);
