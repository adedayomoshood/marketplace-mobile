import { HomeIcon, SettingsIcon, VoucherIcon, WalletIcon } from "@/icons";
import { Flex, SimpleGrid, Stack, Text } from "@chakra-ui/react";
import { Url } from "next/dist/shared/lib/router/router";
import Link from "next/link";
import { NextRouter, useRouter } from "next/router";
import { FunctionComponent } from "react";

export const NavMenu = () => {
  const router: NextRouter = useRouter();

  const isActive = (url: Url) => router.pathname === url;

  return (
    <SimpleGrid
      gap={1}
      as="nav"
      bg="white"
      columns={4}
      width="full"
      boxShadow="0px -1px 50px 10px rgba(0, 0, 0, 0.05)"
    >
      <NavMenuItem
        url="/"
        label="Home"
        icon={HomeIcon}
        isActive={isActive}
      />

      <NavMenuItem
        url="/voucher"
        label="Voucher"
        icon={VoucherIcon}
        isActive={isActive}
      />

      <NavMenuItem
        url="/wallet"
        label="Wallet"
        icon={WalletIcon}
        isActive={isActive}
      />

      <NavMenuItem
        url="/settings"
        label="Settings"
        icon={SettingsIcon}
        isActive={isActive}
      />
    </SimpleGrid>
  );
};

type NavMenuItemProps = {
  url: Url;
  label: string;
  icon: FunctionComponent;
  isActive: (url: Url) => boolean;
};

const NavMenuItem = ({ url, label, icon, isActive }: NavMenuItemProps) => (
  <Link href={url} passHref>
    <Stack p={4} alignItems="center" justifyContent="center">
      <Flex as={icon} fill={isActive(url) ? 'teal.400': 'gray.500'} />

      <Text color={isActive(url) ? "gray.800" : "gray.500"} noOfLines={1}>
        {label}
      </Text>
    </Stack>
  </Link>
);
