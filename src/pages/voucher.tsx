import { Layout, NavMenu } from "@/components";
import { Grid, GridItem, Heading } from "@chakra-ui/react";
import Head from "next/head";

export default function Voucher() {
  return (
    <Layout>
      <Head>
        <title>Voucher &bull; Marketplace Mobile Demo</title>
      </Head>

      <Grid h="100svh" gridTemplateRows="1fr max-content" mx="auto">
        <GridItem overflowY="auto">
          <Heading>Voucher</Heading>
        </GridItem>

        <NavMenu />
      </Grid>
    </Layout>
  );
}
