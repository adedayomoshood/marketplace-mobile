# Marketplace Mobile

A Front end engineering take home challenge from [Moniepoint](https://moniepoint.com/). The project is a web application that displays a list of products from a mock json.

## Project Details

**Live Demo:** - [https://marketplace.moshood.dev/](https://marketplace.moshood.dev/)

## Requirements

To get this application up and running. You need to have the following tools on your machine

- nodejs
- yarn or npm

## Starting the app

In order to start the aplication, you need to run the following commands one after the other

    $ yarn
    $ yarn dev

If the app starts successfully, you will see the following in your terminal

    Compiled successfully!

    You can now view marketplace in the browser.

      Local:            http://localhost:3000
      On Your Network:  http://***.***.***.***:3000

    Note that the development build is not optimized.
    To create a production build, use yarn build.

**Frontend Url:** `http://localhost:3000`

## Tools

This app was bootstraped with **create-next-app** and uses the following tools.

![TypeScript](https://img.shields.io/badge/typescript-%23007ACC.svg?style=for-the-badge&logo=typescript&logoColor=white)
![React](https://img.shields.io/badge/react-%2320232a.svg?style=for-the-badge&logo=react&logoColor=%2361DAFB)
![React](https://img.shields.io/badge/next.js-%23000000.svg?style=for-the-badge&logo=vercel&logoColor=%23FFFFFF)
![Chakra](https://img.shields.io/badge/chakra-%234ED1C5.svg?style=for-the-badge&logo=chakraui&logoColor=white)
