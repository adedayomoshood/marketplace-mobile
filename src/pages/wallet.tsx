import { Layout, NavMenu } from "@/components";
import { Grid, GridItem, Heading } from "@chakra-ui/react";
import Head from "next/head";

export default function Wallet() {
  return (
    <Layout>
      <Head>
        <title>Wallet &bull; Marketplace Mobile Demo</title>
      </Head>

      <Grid h="100svh" gridTemplateRows="1fr max-content" mx="auto">
        <GridItem overflowY="auto">
          <Heading>Wallet</Heading>
        </GridItem>

        <NavMenu />
      </Grid>
    </Layout>
  );
}
