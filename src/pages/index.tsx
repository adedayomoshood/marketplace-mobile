import { Header, Layout, NavMenu, ProductGrid } from "@/components";
import {
  Box,
  Container,
  Flex,
  Grid,
  HStack,
  Image,
  Heading,
  SimpleGrid,
  Stack,
  Text,
  VStack,
} from "@chakra-ui/react";
import Head from "next/head";
import Link from "next/link";
import { useEffect, useMemo, useState } from "react";

export default function Home() {
  return (
    <Layout>
      <Head>
        <title>Marketplace Mobile Demo</title>
      </Head>

      <Grid h="100svh" gridTemplateRows="1fr max-content" mx="auto">
        <Stack
          bg="gray.50"
          overflowY="auto"
          sx={{ scrollSnapType: "y" }}
          spacing={0}
        >
          <Flex flexDir="column" pos="relative">
            <Header hasSearch hasCommentButtom />

            <Hero />
            <CategorySection />
          </Flex>

          <Stack spacing={2} scrollSnapAlign="center">
            <Flex
              p={4}
              top={0}
              zIndex={2}
              overflow="hidden"
              bg="gray.50"
              position="sticky"
              _before={{
                top: 0,
                left: 0,
                zIndex: -1,
                w: "100%",
                height: "0",
                content: "''",
                display: "block",
                position: "absolute",
                boxShadow: "0px 0px 30px 2px rgba(0,0,0,0.2)",
              }}
            >
              <Heading
                as="h2"
                size="md"
                fontWeight="medium"
                display="flex"
                alignItems="center"
              >
                <Box as="span">Best Sale Products</Box>
              </Heading>

              <Text
                as={Link}
                ml="auto"
                fontSize="md"
                href="/products"
                color="teal.400"
                fontWeight="medium"
                aria-label="See more products"
                _hover={{
                  textDecoration: "underline",
                }}
              >
                See more
              </Text>
            </Flex>

            <Box px={4} pb={8} zIndex={1}>
              <ProductGrid />
            </Box>
          </Stack>
        </Stack>

        <NavMenu />
      </Grid>
    </Layout>
  );
}

const CategorySection = () => (
  <SimpleGrid
    px={4}
    py={6}
    gap={0.5}
    bg="white"
    columns={5}
    position="relative"
  >
    <VStack justifyContent="center" spacing={2}>
      <Box w={12} h={12} bg="gray.300" borderRadius="base" />
      <Text fontSize="sm" color="gray.600" lineHeight="24px">
        Category
      </Text>
    </VStack>

    <VStack justifyContent="center" spacing={2}>
      <Box w={12} h={12} bg="gray.300" borderRadius="base" />
      <Text fontSize="sm" color="gray.600" lineHeight="24px">
        Flight
      </Text>
    </VStack>

    <VStack justifyContent="center" spacing={2}>
      <Box w={12} h={12} bg="gray.300" borderRadius="base" />
      <Text fontSize="sm" color="gray.600" lineHeight="24px">
        Bill
      </Text>
    </VStack>

    <VStack justifyContent="center" spacing={2}>
      <Box w={12} h={12} bg="gray.300" borderRadius="base" />
      <Text fontSize="sm" color="gray.600" lineHeight="24px">
        Data Plan
      </Text>
    </VStack>

    <VStack justifyContent="center" spacing={2}>
      <Box w={12} h={12} bg="gray.300" borderRadius="base" />
      <Text fontSize="sm" color="gray.600" lineHeight="24px">
        Top Up
      </Text>
    </VStack>
  </SimpleGrid>
);

const Hero = () => {
  const [currentSlide, setCurrentSlide] = useState(0);
  const images = useMemo(
    () => [
      "/images/slide-1.png",
      "/images/slide-2.png",
      "/images/slide-1.png",
      "/images/slide-2.png",
    ],
    []
  );

  useEffect(() => {
    const interval = setInterval(() => {
      setCurrentSlide((currentSlide + 1) % images.length);
    }, 3000);

    return () => clearInterval(interval);
  }, [currentSlide, images]);

  return (
    <Box position="relative" overflow="hidden" minH={40}>
      <Box
        display="flex"
        transition="transform 0.5s ease-in-out"
        transform={`translateX(-${currentSlide * 100}%)`}
      >
        {images.map((image, index) => (
          <Image
            key={image}
            src={image}
            alt={`Image ${index}`}
            w="100%"
            h="100%"
          />
        ))}
      </Box>
    </Box>
  );
};
