import { Icon, IconProps } from "@chakra-ui/react";

export const BagIcon = ({
  w = 6,
  h = 6,
  stroke = "gray.500",
  ...props
}: IconProps) => (
  <Icon w={w} h={w} {...props} stroke={stroke} fill="none" viewBox="0 0 24 24">
    <path
      strokeLinecap="round"
      strokeWidth="2"
      d="M8 8V7a4 4 0 014-4v0a4 4 0 014 4v1M15 14v-2M9 14v-2"
    ></path>
    <path
      strokeWidth="2"
      d="M4 12c0-1.886 0-2.828.586-3.414C5.172 8 6.114 8 8 8h8c1.886 0 2.828 0 3.414.586C20 9.172 20 10.114 20 12v1c0 3.771 0 5.657-1.172 6.828C17.657 21 15.771 21 12 21v0c-3.771 0-5.657 0-6.828-1.172C4 18.657 4 16.771 4 13v-1z"
    ></path>
  </Icon>
);
