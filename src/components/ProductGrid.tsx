import { Grid } from "@chakra-ui/react";
import { ProductGridItem } from "./ProductGridItem";

export const ProductGrid = () => {
  return (
    <Grid gap={4} gridTemplateColumns="repeat(auto-fill, minmax(170px, 1fr))">
      {mockProducts.map((product) => (
        <ProductGridItem key={product.id} product={product} />
      ))}
    </Grid>
  );
};

export const mockProducts = [
  {
    id: "4ec3c438-d458-11ec-afed-0242ac130002",
    name: "Men's Classic Leather Shoes",
    slug: "mens-classic-leather-shoes",
    imageUrl: "/images/poduct-1.png",
    isInWishlist: false,
    meta: {
      price: 99.99,
      category: "Shoes",
    },
    rating: {
      averageRating: 4.7,
      totalRating: 112,
    },
  },
  {
    id: "72d1c8bc-d458-11ec-afed-0242ac130002",
    name: "Women's Floral Print Dress",
    slug: "womens-floral-print-dress",
    imageUrl: "/images/poduct-2.png",

    isInWishlist: true,
    meta: {
      price: 69.99,
      category: "Dress",
    },
    rating: {
      averageRating: 4.5,
      totalRating: 87,
    },
  },
  {
    id: "96a932a6-d458-11ec-afed-0242ac130002",
    name: "Men's Plaid Button-Up Shirt",
    slug: "mens-plaid-button-up-shirt",
    imageUrl: "/images/poduct-3.png",

    isInWishlist: false,
    meta: {
      price: 49.99,
      category: "Shirts",
    },
    rating: {
      averageRating: 4.2,
      totalRating: 64,
    },
  },
  {
    id: "b8e6b692-d458-11ec-afed-0242ac130002",
    name: "Women's High-Waisted Jeans",
    slug: "womens-high-waisted-jeans",
    imageUrl: "/images/poduct-4.png",

    isInWishlist: true,
    meta: {
      price: 79.99,
      category: "Jeans",
    },
    rating: {
      averageRating: 4.8,
      totalRating: 92,
    },
  },
  {
    id: "d3a90a7c-d458-11ec-afed-0242ac130002",
    name: "Men's Leather Belt",
    slug: "mens-leather-belt",
    imageUrl: "/images/poduct-5.png",

    isInWishlist: false,
    meta: {
      price: 29.99,
      category: "Accessories",
    },
    rating: {
      averageRating: 4.3,
      totalRating: 43,
    },
  },
  {
    id: "0c23944a-d459-11ec-afed-0242ac130002",
    name: "Men's Slim-Fit T-Shirt",
    slug: "mens-slim-fit-t-shirt",
    imageUrl: "/images/poduct-1.png",
    isInWishlist: false,
    meta: {
      price: 24.99,
      category: "Shirts",
    },
    rating: {
      averageRating: 4.1,
      totalRating: 56,
    },
  },
  {
    id: "34ba1e6c-d459-11ec-afed-0242ac130002",
    name: "Women's Leather Jacket",
    slug: "womens-leather-jacket",
    imageUrl: "/images/poduct-2.png",
    isInWishlist: false,
    meta: {
      price: 199.99,
      category: "Jackets",
    },
    rating: {
      averageRating: 4.9,
      totalRating: 125,
    },
  },
  {
    id: "5617d4a4-d459-11ec-afed-0242ac130002",
    name: "Men's Cargo Shorts",
    slug: "mens-cargo-shorts",
    imageUrl: "/images/poduct-3.png",
    isInWishlist: false,
    meta: {
      price: 54.99,
      category: "Shorts",
    },
    rating: {
      averageRating: 4.4,
      totalRating: 36,
    },
  },
  {
    id: "79d2f3dc-d459-11ec-afed-0242ac130002",
    name: "Women's Striped Jumpsuit",
    slug: "womens-striped-jumpsuit",
    imageUrl: "/images/poduct-4.png",
    isInWishlist: true,
    meta: {
      price: 89.99,
      category: "Jumpsuits",
    },
    rating: {
      averageRating: 4.7,
      totalRating: 79,
    },
  },
  {
    id: "9d84b9c6-d459-11ec-afed-0242ac130002",
    name: "Men's Denim Jacket",
    slug: "mens-denim-jacket",
    imageUrl: "/images/poduct-5.png",
    isInWishlist: false,
    meta: {
      price: 129.99,
      category: "Jackets",
    },
    rating: {
      averageRating: 4.3,
      totalRating: 67,
    },
  },
  {
    id: "b94c7e82-e54d-11ec-9a03-0242ac130003",
    name: "Women's Maxi Dress",
    slug: "womens-maxi-dress",
    imageUrl: "/images/poduct-1.png",
    isInWishlist: false,
    meta: {
      price: 79.99,
      category: "Dresses",
    },
    rating: {
      averageRating: 4.6,
      totalRating: 92,
    },
  },
  {
    id: "2c860112-e54e-11ec-9a03-0242ac130003",
    name: "Men's Chino Pants",
    slug: "mens-chino-pants",
    imageUrl: "/images/poduct-2.png",
    isInWishlist: true,
    meta: {
      price: 59.99,
      category: "Pants",
    },
    rating: {
      averageRating: 4.2,
      totalRating: 68,
    },
  },
  {
    id: "64e4e3c4-e54e-11ec-9a03-0242ac130003",
    name: "Women's Peplum Top",
    slug: "womens-peplum-top",
    imageUrl: "/images/poduct-3.png",
    isInWishlist: true,
    meta: {
      price: 34.99,
      category: "Tops",
    },
    rating: {
      averageRating: 4.0,
      totalRating: 42,
    },
  },
  {
    id: "9c8a2d70-e54e-11ec-9a03-0242ac130003",
    name: "Men's Oxford Shirt",
    slug: "mens-oxford-shirt",
    imageUrl: "/images/poduct-4.png",
    isInWishlist: false,
    meta: {
      price: 49.99,
      category: "Shirts",
    },
    rating: {
      averageRating: 4.4,
      totalRating: 56,
    },
  },
  {
    id: "d8c5c364-e54e-11ec-9a03-0242ac130003",
    name: "Women's Denim Skirt",
    slug: "womens-denim-skirt",
    imageUrl: "/images/poduct-5.png",
    isInWishlist: false,
    meta: {
      price: 44.99,
      category: "Skirts",
    },
    rating: {
      averageRating: 4.8,
      totalRating: 36,
    },
  },
];
