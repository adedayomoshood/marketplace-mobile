import { HeartIcon, StarIcon } from "@/icons";
import {
  Stack,
  Flex,
  Heading,
  HStack,
  StackDivider,
  Text,
  Box,
} from "@chakra-ui/react";
import Image from "next/image";
import Link from "next/link";

export type ProductGridItemType = {
  id: string;
  name: string;
  slug?: string;
  isInWishlist?: boolean;
  meta: {
    price: number;
    salePrice?: number;
    category: string;
  };
  rating: Rating;
  imageUrl: string;
};

type Rating = {
  averageRating: number;
  totalRating: number;
};

type ProductGridItemProps = {
  product: ProductGridItemType;
};

export const ProductGridItem = ({ product }: ProductGridItemProps) => {
  return (
    <Stack
      as={Link}
      spacing={0}
      overflow="hidden"
      href={`/products/${product.slug}`}
      transitionDuration="slow"
      _hover={{
        boxShadow: "0px 0px 30px 2px rgba(0,0,0,0.1)",
      }}
    >
      <Flex bg="gray.300" sx={{ aspectRatio: "16/9" }} position="relative">
        <Box>
          <Image
            fill
            alt=""
            src={product.imageUrl}
            style={{ objectFit: "cover" }}
          />
        </Box>
        <HeartIcon
          top={2}
          right={2}
          position="absolute"
          stroke={product.isInWishlist ? "red.400" : "gray.600"}
          fill={product.isInWishlist ? "red.400" : "none"}
        />
      </Flex>

      <Stack p={3} flex={1}>
        <Text color="gray.500" fontSize="sm" lineHeight={1}>
          {product.meta.category}
        </Text>

        <Heading as="h3" size="sm" flex={1} fontWeight="medium" noOfLines={2}>
          {product.name}
        </Heading>

        <HStack justifyContent="space-between" mt="auto">
          <HStack spacing={1}>
            <StarIcon w={4} h={4} />

            <HStack
              spacing={1}
              divider={<StackDivider borderColor="gray.300" />}
            >
              <Text color="gray.500" fontSize="xs" lineHeight={1}>
                {product.rating.averageRating}
              </Text>

              <Text color="gray.500" fontSize="xs" lineHeight={1}>
                {product.rating.totalRating}
              </Text>
            </HStack>
          </HStack>

          <Text color="teal.400" fontWeight="semibold" fontSize="lg">
            ${Number(product.meta.price).toFixed(2)}
          </Text>
        </HStack>
      </Stack>
    </Stack>
  );
};
