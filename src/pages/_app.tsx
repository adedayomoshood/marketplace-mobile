import { ChakraProvider, Container } from "@chakra-ui/react";
import { AnimatePresence } from "framer-motion";
import type { AppProps } from "next/app";

export default function App({ Component, pageProps, router }: AppProps) {
  return (
    <ChakraProvider>
      <AnimatePresence
        mode="wait"
        initial={false}
        onExitComplete={() => window.scrollTo(0, 0)}
      >
        <Container p={0} maxW="container.sm">
          <Component {...pageProps} key={router.asPath} />
        </Container>
      </AnimatePresence>
    </ChakraProvider>
  );
}
