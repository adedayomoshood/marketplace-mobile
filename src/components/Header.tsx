import {
  SearchIcon,
  BagIcon,
  CommentsIcon,
  HeartIcon,
  ShareIcon,
  ChevronLeftIcon,
} from "@/icons";
import {
  Flex,
  InputGroup,
  InputLeftElement,
  Input,
  IconButton,
  Badge,
  Box,
  Text,
} from "@chakra-ui/react";
import { useRouter } from "next/router";

type HeaderProps = {
  hasSearch?: boolean;
  isRelative?: boolean;
  hasBackButton?: boolean;
  hasShareButton?: boolean;
  hasWishListButton?: boolean;
  hasCommentButtom?: boolean;
};

export const Header = ({
  hasSearch,
  isRelative,
  hasBackButton,
  hasShareButton,
  hasWishListButton,
  hasCommentButtom,
}: HeaderProps) => {
  const router = useRouter();

  return (
    <Flex
      p={4}
      gap={4}
      top={0}
      w="100%"
      zIndex={2}
      alignItems="center"
      bg={isRelative ? "white" : "transparent"}
      position={isRelative ? "relative" : "absolute"}
      justifyContent="flex-end"
    >
      {hasBackButton && (
        <IconButton
          variant="unstyled"
          p={0}
          minW={6}
          aria-label="Back"
          icon={<ChevronLeftIcon fill="gray.400" />}
          onClick={router.back}
          mr="auto"
        />
      )}

      {hasSearch && (
        <InputGroup>
          <InputLeftElement pointerEvents="none" alignItems="center">
            <SearchIcon />
          </InputLeftElement>

          <Input
            type="search"
            fontSize="sm"
            color="gray.600"
            borderRadius="lg"
            placeholder="Search"
            borderColor="blackAlpha.300"
            _placeholder={{ color: "blackAlpha.400" }}
          />
        </InputGroup>
      )}

      {hasWishListButton && (
        <IconButton
          variant="unstyled"
          p={0}
          minW={6}
          aria-label="Add to Wishlist"
          icon={<HeartIcon />}
        />
      )}

      {hasShareButton && (
        <IconButton
          variant="unstyled"
          p={0}
          minW={6}
          aria-label="Share"
          icon={<ShareIcon />}
        />
      )}

      <Flex alignItems="flex-start" position="relative">
        <IconButton
          p={0}
          minW={6}
          variant="unstyled"
          aria-label="Shopping Bag"
          icon={<BagIcon w={7} h={7} />}
        />
        <Flex
          h={5}
          px={1}
          minW={5}
          left={"50%"}
          bg="red.500"
          alignItems="center"
          justifyContent="center"
          borderRadius="base"
          position="absolute"
        >
          <Text
            fontSize="xs"
            color="white"
            lineHeight={1}
            fontWeight="semibold"
          >
            1
          </Text>
        </Flex>
      </Flex>

      {hasCommentButtom && (
        <Flex alignItems="flex-start" position="relative">
          <IconButton
            variant="unstyled"
            p={0}
            minW={6}
            aria-label="Comments"
            icon={<CommentsIcon w={7} h={7} />}
          />
          <Flex
            h={5}
            px={1}
            minW={5}
            left={"50%"}
            bg="red.500"
            alignItems="center"
            justifyContent="center"
            borderRadius="base"
            position="absolute"
          >
            <Text
              fontSize="xs"
              color="white"
              lineHeight={1}
              fontWeight="semibold"
            >
              9+
            </Text>
          </Flex>
        </Flex>
      )}
    </Flex>
  );
};
