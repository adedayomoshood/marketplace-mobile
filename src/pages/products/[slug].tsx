import {
  BuyNow,
  Header,
  Layout,
  ProductGridItemType,
  mockProducts,
} from "@/components";
import { StarIcon, StoreIcon } from "@/icons";
import {
  Box,
  Grid,
  GridItem,
  HStack,
  Heading,
  SimpleGrid,
  Stack,
  StackDivider,
  Tab,
  TabList,
  TabPanel,
  TabPanels,
  Tabs,
  Text,
} from "@chakra-ui/react";
import Head from "next/head";
import Image from "next/image";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";

export default function SingleProduct() {
  const router = useRouter();
  const [product, setProduct] = useState<ProductGridItemType | null>();

  const { slug } = router?.query;

  useEffect(() => {
    if (typeof window !== undefined) {
      const product = mockProducts.find((item) => item.slug === slug);

      setProduct(product);
    }
  }, [router, slug]);

  return (
    <Layout>
      <Head>
        <title>{product?.name}</title>
      </Head>

      <Grid h="100svh" gridTemplateRows="1fr max-content" mx="auto">
        <GridItem overflowY="auto">
          <Header hasBackButton hasShareButton hasWishListButton isRelative />

          <Stack spacing={4} p={4}>
            <ImageGallery imageUrl={product?.imageUrl} />

            <Stack>
              <HStack spacing={1}>
                <StoreIcon w={4} h={4} />
                <Text color="gray.500" fontSize="sm">
                  tokobaju.id
                </Text>
              </HStack>

              <Heading as="h1" size="md" fontWeight="semibold">
                {product?.name}
              </Heading>
            </Stack>

            <Stack
              spacing={4}
              flexWrap="wrap"
              direction="row"
              color="gray.500"
              divider={
                <StackDivider border="none" alignItems="center" display="flex">
                  <Box w={1} h={1} bg="gray.400" borderRadius="full" />
                </StackDivider>
              }
            >
              <HStack spacing={1}>
                <StarIcon w={4} h={4} />
                <Text>{product?.rating.averageRating} Rating</Text>
              </HStack>

              <Text>{product?.rating.totalRating} Reviews</Text>
              <Text>2.9k Sold</Text>
            </Stack>

            <ProductDescription />
          </Stack>
        </GridItem>

        <BuyNow price={27.5} />
      </Grid>
    </Layout>
  );
}

const ImageGallery = ({ imageUrl }: { imageUrl?: string }) => {
  return (
    <Box
      h={96}
      bg="gray.200"
      overflow="hidden"
      borderRadius="base"
      position="relative"
      maxWidth="initial"
    >
      {imageUrl && (
        <Image
          fill
          alt=""
          priority
          src={imageUrl}
          style={{ objectFit: "cover" }}
        />
      )}

      <Stack position="absolute" top={0} left={0} p={4}>
        <Box
          bg="red"
          w={12}
          h={12}
          position="relative"
          borderRadius="base"
          overflow="hidden"
          objectFit="cover"
          outline="2px solid"
          outlineColor="rgba(0,0,0,0.5)"
        >
          <Image
            width="48"
            height="48"
            alt=""
            src="https://picsum.photos/48/48"
            style={{ objectFit: "cover" }}
          />
        </Box>
        <Box
          bg="red"
          w={12}
          h={12}
          position="relative"
          borderRadius="base"
          overflow="hidden"
          objectFit="cover"
          outline="2px solid"
          outlineColor="rgba(0,0,0,0.5)"
        >
          <Image
            width="48"
            height="48"
            alt=""
            src="https://picsum.photos/40/40"
            style={{ objectFit: "cover" }}
          />
        </Box>
        <Box
          bg="red"
          w={12}
          h={12}
          position="relative"
          borderRadius="base"
          overflow="hidden"
          objectFit="cover"
          outline="2px solid"
          outlineColor="rgba(0,0,0,0.5)"
        >
          <Image
            width="48"
            height="48"
            alt=""
            src="https://picsum.photos/seed/40/40"
            style={{ objectFit: "cover" }}
          />
        </Box>
        <Box
          bg="red"
          w={12}
          h={12}
          position="relative"
          borderRadius="base"
          overflow="hidden"
          objectFit="cover"
          outline="2px solid"
          outlineColor="rgba(0,0,0,0.5)"
        >
          <Image
            width="48"
            height="48"
            alt=""
            src="https://picsum.photos/44/44"
            style={{ objectFit: "cover" }}
          />
        </Box>
      </Stack>
    </Box>
  );
};

const ProductDescription = () => (
  <Tabs colorScheme="teal">
    <TabList>
      <Tab
        fontWeight="medium"
        color="gray.500"
        _selected={{ color: "teal.400", borderBottomColor: "teal.400" }}
      >
        About Item
      </Tab>
      <Tab
        fontWeight="medium"
        color="gray.500"
        _selected={{ color: "teal.400", borderBottomColor: "teal.400" }}
      >
        Reviews
      </Tab>
    </TabList>

    <TabPanels>
      <TabPanel>
        <Stack gap={6} divider={<StackDivider />}>
          <SimpleGrid columns={2} gap={2}>
            <Text gap={3}>
              <Text as="span" color="gray.500" fontSize="sm">
                Brand:
              </Text>
              <Text
                as="span"
                display="inline-block"
                ml={3}
                fontSize="md"
                fontWeight="medium"
              >
                One
              </Text>
            </Text>
            <Text gap={3}>
              <Text as="span" color="gray.500" fontSize="sm">
                Color:
              </Text>
              <Text
                as="span"
                display="inline-block"
                ml={3}
                fontSize="md"
                fontWeight="medium"
              >
                Burgundy
              </Text>
            </Text>
            <Text gap={3}>
              <Text as="span" color="gray.500" fontSize="sm">
                Category:
              </Text>
              <Text
                as="span"
                display="inline-block"
                ml={3}
                fontSize="md"
                fontWeight="medium"
              >
                Clothing
              </Text>
            </Text>
            <Text gap={3}>
              <Text as="span" color="gray.500" fontSize="sm">
                Material:
              </Text>
              <Text
                as="span"
                display="inline-block"
                ml={3}
                fontSize="md"
                fontWeight="medium"
              >
                Cotton
              </Text>
            </Text>
            <Text gap={3}>
              <Text as="span" color="gray.500" fontSize="sm">
                Condition:
              </Text>
              <Text
                as="span"
                display="inline-block"
                ml={3}
                fontSize="md"
                fontWeight="medium"
              >
                New
              </Text>
            </Text>
            <Text gap={3}>
              <Text as="span" color="gray.500" fontSize="sm">
                Heavy:
              </Text>
              <Text
                as="span"
                display="inline-block"
                ml={3}
                fontSize="md"
                fontWeight="medium"
              >
                200g
              </Text>
            </Text>
          </SimpleGrid>

          <Stack>
            <Text fontSize="sm" color="gray.500">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
              reprehenderit in voluptate velit esse cillum dolore eu fugiat
              nulla pariatur. Excepteur sint occaecat cupidatat non proident,
              sunt in culpa qui officia deserunt mollit anim id est laborum.
            </Text>
          </Stack>
        </Stack>
      </TabPanel>

      <TabPanel>
        <Heading size="sm" fontWeight="medium">
          Reviews
        </Heading>
      </TabPanel>
    </TabPanels>
  </Tabs>
);
