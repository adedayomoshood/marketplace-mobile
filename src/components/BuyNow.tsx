import { BagIcon } from "@/icons";
import {
  Button,
  Flex,
  Text,
  VStack
} from "@chakra-ui/react";

type BuyNowProps = {
  price: number;
  addToCart?: number;
};

export const BuyNow = ({ price, addToCart }: BuyNowProps) => {
  return (
    <Flex
      p={4}
      gap={2}
      bg="white"
      width="full"
      alignItems="center"
      justifyContent="space-between"
      boxShadow="0px -1px 50px 10px rgba(0, 0, 0, 0.05)"
    >
      <VStack justifyContent="flex-start" alignItems="flex-start" spacing={1}>
        <Text fontSize="sm" lineHeight={1.25} color="gray.500">
          Total Price
        </Text>

        <Text
          fontSize="xl"
          color="teal.400"
          lineHeight={1.25}
          fontWeight="semibold"
        >
          ${Number(price).toFixed(2)}
        </Text>
      </VStack>

      <Flex alignItems="stretch" borderRadius="md" overflow="hidden">
        <Flex bg="teal.400" color="white" alignItems="center" gap={2} p={4}>
          <BagIcon stroke="white" />
          <Text>1</Text>
        </Flex>

        <Button
          h="14"
          px={8}
          size="lg"
          ml="auto"
          color="white"
          bg="gray.700"
          fontWeight="medium"
          borderRadius={0}
          fontSize="md"
        >
          Buy Now
        </Button>
      </Flex>
    </Flex>
  );
};
