import { Icon, IconProps } from "@chakra-ui/react";

export const SearchIcon = ({
  w = 6,
  h = 6,
  stroke = "gray.400",
  ...props
}: IconProps) => (
  <Icon w={w} h={h} stroke={stroke} {...props} fill="none" viewBox="0 0 24 24">
    <circle cx="11" cy="11" r="7" strokeWidth="2" />
    <path
      strokeLinecap="round"
      strokeWidth="2"
      d="M20 20l-3-3"
    />
  </Icon>
);

