import { Header, Layout, NavMenu, ProductGrid } from "@/components";
import { Grid, GridItem, Flex, Stack, Box, Heading } from "@chakra-ui/react";
import Head from "next/head";

export default function Products() {
  return (
    <Layout>
      <Head>
        <title>All Products &bull; Marketplace Mobile Demo</title>
      </Head>

      <Grid h="100vh" gridTemplateRows="1fr max-content" mx="auto" bg="gray.50">
        <GridItem overflowY="auto">
          <Header hasBackButton hasSearch hasCommentButtom isRelative />

          <Stack spacing={2}>
            <Flex p={4} top={0} zIndex={2}>
              <Heading
                as="h2"
                size="md"
                fontWeight="medium"
              >
                <Box as="span">All Products</Box>
              </Heading>
            </Flex>

            <Box px={4} pb={8} zIndex={1}>
              <ProductGrid />
            </Box>
          </Stack>
        </GridItem>

        <NavMenu />
      </Grid>
    </Layout>
  );
}
