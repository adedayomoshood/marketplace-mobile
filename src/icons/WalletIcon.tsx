import { IconProps, Icon } from "@chakra-ui/react";

export const WalletIcon= ({
  w = 6,
  h = 6,
  fill = "gray.400",
  ...props
}: IconProps) => (
  <Icon w={w} h={h} fill={fill} {...props} viewBox="0 0 24 24">
    <g clipPath="url(#clip0_1020_44)">
      <path d="M4 2.667a2.67 2.67 0 00-2.667 2.666v13.334A2.67 2.67 0 004 21.333h16a2.67 2.67 0 002.667-2.666V9.333A2.67 2.67 0 0020 6.667H4.667A.669.669 0 014 6c0-.367.3-.667.667-.667H20c.738 0 1.334-.596 1.334-1.333 0-.738-.596-1.333-1.334-1.333H4zm14.667 10a1.333 1.333 0 110 2.665 1.333 1.333 0 010-2.665z" />
    </g>
    <defs>
      <clipPath id="clip0_1020_44">
        <path
          fill="#fff"
          d="M0 0H21.333V21.333H0z"
          transform="translate(1.333 1.333)"
        />
      </clipPath>
    </defs>
  </Icon>
);
